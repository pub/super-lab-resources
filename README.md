# DFF (Super Lab) Inventory
https://docs.google.com/spreadsheets/d/13cw3a9YDh2BIY7_ZjBSAamfUx3LGF4zoPCG7DPd5TAw/edit?usp=sharing


# How to:
- ### Planning
    - Export Control: Can this inventory be exported from the US to the target country?  If not what is the "work around"?
    - How are we going to ship?
        - Vendor dropship
            - Can they do it?
            - Have they shipped to this country before?
            - What is their planned route?  Air/sea/truck/combination?  
            - Remember, many times we understand the implications of shipping to a particular place better than they do
        - Adcom or other logistics company shipping    
            - Expected lead time?
            - Cost?
            - Size and weight restrictions to that country?
            - Route?  Air/sea/truck/combination?
    - Duty and Import
        - What entity is importing?
        - Do they have or can they figure out a way to get duty-free import (usually $100K in savings)
    - Utility Considerations
        - Electrical Specs (110V/220V/440V 50Hz/60Hz Single or Three Phase)
        - Water Quality and temperature (used to determine the need for a chiller and filtration system with the Waterjet)
        - Lighting: If there's a lack of natural light will there be enough lighting for safe lab operation?
        - Ventilation
            - How will the laser cutters be vented?  Where will those vents exhaust to?
            - Welding: Do we need a trunk?
            - Molding and Casting: Can't be an enclosed space
            - In the case of a biolab: Is a laminar flow hood needed?  What is the "natural ventilation" like in the selected space?
    - Layout
        - Work together on a lab layout to help determine space, utility needs, renovation needs
    - Machines 
        - Goals of the lab and who will be its primary users?
        - What will the lab included?  (Balance of budget and wishlist)
        - Will all purchases be made at once or will it be a multi-phase purchase/deployment?  If multi-phase determine the priorities and purchasing order.

- ### Purchasing
- ### Deployment
- ### Installation
- ### Operation
    - Gitlab
        - Documenting users, training, spaces, tools
        - Repos to document project work
    - Each Machine should have an owner who is responsible for learning that machine, training others, performing maintenance and overseeing necessary repairs.
    - Machine Maintenance is absolutely necessary.  Creating a schedule in Gitlab can help to ensure this is being done regularly.  The are high level, precise machines that don't like to sit idle or not be maintained.

